﻿using System;
using System.IO;

namespace CLXParser.Code
{
    /// <summary>
    /// Logger realizes "Singleton" Pattern
    /// </summary>
    internal class Logger : IDisposable
    {
        //inner object
        private static Logger logger;

        private readonly string path;
        private readonly string filename;
        private readonly FileStream fs;
        private readonly TextWriter writer;

        /// <summary>
        /// Used for create logger once
        /// </summary>
        /// <param name="path">Path to logger folder</param>
        /// <returns>Logger</returns>
        internal static Logger InitializeLogger(string path)
        {
            if (!Directory.Exists(path + '\\' + "Log")) Directory.CreateDirectory(path + '\\' + "Log");

            if (logger == null)
            {
                logger = new Logger(path + '\\' + "Log");
            }
            
            return GetLogger();
        }

        /// <summary>
        /// Return initialized logger
        /// </summary>
        /// <returns>Null if logger isn't initialized</returns>
        internal static Logger GetLogger() 
        {
            return logger;
        }

        /// <summary>
        /// Write message to log_file
        /// </summary>
        /// <param name="msg">Our message</param>
        internal void WriteMessage(string msg)
        {
            try
            {
               writer.WriteLine(DateTime.Now.ToString("T") + " " + msg);
            }
            catch (Exception) { }
        }

        //for Disposing our object
        public void Dispose()
        {
            writer.Dispose();
            fs.Dispose();
        }

        // we use private constructor 'cause we don't need contain more than one logger
        private Logger(string pathToLogFile)
        {
            this.path = pathToLogFile;
            filename = DateTime.UtcNow.Date.ToShortDateString() + ".log";
            fs = new FileStream(this.path + '\\' + this.filename, FileMode.Append, FileAccess.Write);
            writer = new StreamWriter(fs);
        }
    }
}
