﻿using System;
using System.IO;
using System.Text;

namespace CLXParser.Code
{
    /// <summary>
    /// Used for writing messages to files
    /// </summary>
    internal class FileWriter : IDisposable
    {
        //Our message separator
        private const char MessageSeparator = '|';

        private readonly string FirstFileName;
        private readonly string SecondFileName;
        private readonly string PathToOutput;

        //Each filestream for each writer
        private readonly FileStream HostFileStream;
        private readonly FileStream VulnersFileStream;

        //Writers for our files
        private readonly StreamWriter WriterForHost;
        private readonly StreamWriter WriterForVulners;

        //Logger object for simple logging
        internal Logger Logger { get; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="PathToOutput">Our path to output</param>
        internal FileWriter(string PathToOutput)
        {
            this.PathToOutput = PathToOutput;
            this.Logger = Logger.InitializeLogger(PathToOutput);
            this.FirstFileName = PathToOutput + '\\' + "soft.txt";
            this.SecondFileName = PathToOutput + '\\' + "vulners.txt";

            //if we have registred files in output - we delete them
            if (File.Exists(FirstFileName)) File.Delete(FirstFileName);
            if (File.Exists(SecondFileName)) File.Delete(SecondFileName);

            //Initialize writers and streams
            HostFileStream = new FileStream(FirstFileName, FileMode.Append, FileAccess.Write);
            VulnersFileStream = new FileStream(SecondFileName, FileMode.Append, FileAccess.Write);
            WriterForHost = new StreamWriter(HostFileStream);
            WriterForVulners = new StreamWriter(VulnersFileStream);
        }

        /// <summary>
        /// Using for writing Host information to file
        /// </summary>
        /// <param name="ip">host's ip</param>
        /// <param name="software">some software from host</param>
        /// <param name="vulner">registred software vulner</param>
        internal void WriteHostInformation(string ip, string software, string vulner)
        {
            WriterForHost.WriteLine($"{ip} {MessageSeparator} {software} {MessageSeparator} {vulner}");
        }

        /// <summary>
        /// Using for writing vulner information to file
        /// </summary>
        /// <param name="vulner">our vulner</param>
        internal void WriteVulnerInformation(string vulner)
        {
            WriterForVulners.WriteLine(vulner);
        }

        //Using for disposing our writers
        public void Dispose()
        {
            try
            {
                //Close the connections from StreamWriters
                WriterForHost.Close();
                WriterForVulners.Close();

                //Disposing StreamWriters
                WriterForHost.Dispose();
                WriterForVulners.Dispose();

                //Disposing FileStreams
                HostFileStream.Dispose();
                VulnersFileStream.Dispose();

                //Disposing logger
                Logger.Dispose();
            }
            catch (EncoderFallbackException EFexc)
            {
                Logger.WriteMessage(EFexc.Message);
            }
        }
    }
}
