﻿namespace CLXParser.Code
{
    /// <summary>
    /// String generator for vulners information
    /// </summary>
    internal static class VulnerStringGenerator
    {

        /// <summary>
        /// Used this method for short information from node in Data->Host->Software->Vulners
        /// </summary>
        /// <param name="id">vulner id</param>
        /// <param name="level">vulner level</param>
        /// <returns>formated string</returns>
        internal static string CreateShortVulnerString(uint id, byte level)
        {
            return $"{id} | {level}";
        }

        /// <summary>
        /// Used this method for generate string from node in Content->Data->Vulners
        /// </summary>
        /// <param name="id">vulner id</param>
        /// <param name="title">title</param>
        /// <param name="short_description">short_description</param>
        /// <param name="description">full description</param>
        /// <param name="howToFix">how to fix problem</param>
        /// <param name="links">links</param>
        /// <param name="cvss_base_csore_decomp">cvss decomp</param>
        /// <param name="global_value">global value</param>
        /// <param name="global_name">global name</param>
        /// <returns>formated string</returns>
        internal static string CreateFullVulnerString(uint id, string title, string short_description, string description,
                                                      string howToFix, string links, string cvss_base_csore_decomp,
                                                      string global_value, string global_name)
        {
            return $"{id} | {title} | {short_description} | {description} | {howToFix} | {links} | {cvss_base_csore_decomp} | {global_value} | {global_name}";
        }
    }
}
