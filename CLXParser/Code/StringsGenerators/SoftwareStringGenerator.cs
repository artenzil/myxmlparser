﻿namespace CLXParser.Code
{
    /// <summary>
    /// Generate string from software information
    /// </summary>
    internal static class SoftwareStringGenerator
    {
        /// <summary>
        /// Generate string from software information
        /// </summary>
        /// <param name="name">Software name</param>
        /// <param name="version">Software version</param>
        /// <returns>Formated string</returns>
        internal static string GenerateSoftware(string name, string version) => $"{name} | {version}";
    }
}
