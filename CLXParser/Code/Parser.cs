﻿using System;
using System.Xml;

namespace CLXParser.Code
{
    /// <summary>
    /// Class uses the "Singleton" Pattern
    /// </summary>
    internal class Parser : IDisposable
    {

        /// <summary>
        /// Return a parser for XML file
        /// </summary>
        /// <param name="pathToXML">Full path to file with '.xml' extension</param>
        /// <param name="pathToOutput">Full path to folder for output results and logger</param>
        /// <returns>Parser instance</returns>
        internal static Parser GetXmlParser(string pathToXML, string pathToOutput)
        {
            if (Instance == null) Instance = new Parser();
            Instance.ChangeOutputPath(pathToOutput);
            Instance.ChangeXMLPath(pathToXML);
            return Instance;
        }

        
        private static Parser Instance;

        //for writer
        private string pathToXML;
        private string pathToOutput;

        //for "host" element in XML file
        private string hostIP;

        //used for write messages to file
        private FileWriter writer;

        /// <summary>
        /// Parse large XML file
        /// </summary>
        /// <returns>true if operation completed successful, false if not</returns>
        internal bool ParseXML()
        {
            bool status = true;
            writer = new FileWriter(pathToOutput);
            writer.Logger.WriteMessage("Parsing started");
            XmlReaderSettings settings = new XmlReaderSettings{ IgnoreWhitespace = true };

            try
            {
                //Create XML Reader for our xml file
                using(XmlReader reader = XmlReader.Create(this.pathToXML, settings))
                {
                    //Working while we haven't reached the EOF
                    while (reader.Read())
                    {
                        if (reader.NodeType == XmlNodeType.Element)
                        {
                            if (reader.Name == "host")
                            {
                                reader.MoveToAttribute("ip");
                                hostIP = reader.Value;
                                writer.Logger.WriteMessage($"Parsing Host with ip - {this.hostIP}");
                            }
                            if (reader.Name == "soft")
                            {
                                writer.Logger.WriteMessage("Parsing software started!");
                                ParseSoftware(reader);
                                writer.Logger.WriteMessage("Parsing software completed!");
                            }
                            if (reader.Name == "vulners")
                            {
                                writer.Logger.WriteMessage("Parsing Vulners");
                                ParseVulners(reader);
                                writer.Logger.WriteMessage("Parsing Vulners complete");
                            }
                        }
                        if (reader.NodeType == XmlNodeType.EndElement)
                        {
                            if (reader.Name == "host") writer.Logger.WriteMessage($"Parsing host:{this.hostIP} completed!");
                        }
                    }
                } 
            }
            //Just for catching any exceptions from working code
            catch (Exception exc) { writer.Logger.WriteMessage(exc.Message); status = false; }
            finally { this.Dispose();}
            return status;
        }

        //Parsing software node
        private void ParseSoftware(XmlReader reader)
        {
            string soft_name = null;
            string soft_version = null;

            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.Element)
                {
                    if (reader.Name == "name")
                    {
                        soft_name = reader.ReadElementContentAsString();
                    }
                    if (reader.Name == "version")
                    {
                        soft_version = reader.ReadElementContentAsString();
                    }
                    if (reader.Name == "vulner")
                    {
                        uint id;
                        byte level;
                        reader.MoveToAttribute("id");
                        id = uint.Parse(reader.Value);
                        reader.MoveToAttribute("level");
                        level = byte.Parse(reader.Value);
                        if (level != 5 && level != 3) continue;

                        writer.WriteHostInformation(this.hostIP,
                                                    SoftwareStringGenerator.GenerateSoftware(soft_name, soft_version),
                                                    VulnerStringGenerator.CreateShortVulnerString(id, level));
                    }
                }
                if (reader.NodeType == XmlNodeType.EndElement)
                {
                    if (reader.Name == "soft")
                    {
                        soft_name = null;
                        soft_version = null;
                        return;
                    }
                }
            }
        }

        //Parsing vulners from Data -> Vulners
        private void ParseVulners(XmlReader reader)
        {
            uint?  id = null;
            string title = null;
            string short_description = null;
            string description = null;
            string how_to_fix = null;
            string links = null;
            string cvss_decomp = null;
            string global_name = null;
            string global_value = null;
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.Element)
                {
                    if (reader.Name == "vulner")
                    {
                        reader.MoveToAttribute("id");
                        id = uint.Parse(reader.Value);
                    }

                    if (reader.Name == "title") title = reader.ReadElementContentAsString();
                    if (reader.Name == "short_description") short_description = reader.ReadElementContentAsString();
                    if (reader.Name == "description") description = reader.ReadElementContentAsString();
                    if (reader.Name == "how_to_fix") how_to_fix = reader.ReadElementContentAsString();
                    if (reader.Name == "links") links = reader.ReadElementContentAsString();

                    if (reader.Name == "cvss")
                    {
                        reader.MoveToAttribute("base_score_decomp");
                        cvss_decomp = reader.Value;
                    }

                    if (reader.Name == "global_id")
                    {
                        reader.MoveToAttribute("name");
                        global_name = reader.Value;
                        reader.MoveToAttribute("value");
                        global_value = reader.Value;
                    }
                }
                if (reader.NodeType == XmlNodeType.EndElement)
                {
                    if (reader.Name == "vulner")
                    {
                        if (cvss_decomp == null || global_name == null || global_value == null || !id.HasValue) continue;
                        writer.WriteVulnerInformation(
                                VulnerStringGenerator.CreateFullVulnerString(id.Value, title, short_description, description, how_to_fix, links,
                                                                            cvss_decomp, global_value, global_name)
                                                      );

                        id = null;
                        title = null;
                        short_description = null;
                        description = null;
                        how_to_fix = null;
                        links = null;
                        cvss_decomp = null;
                        global_name = null;
                        global_value = null;
                    }
                    if (reader.Name == "vulners") return;
                }
            }
        }

        //empty constructor 
        private Parser(){}

        private void ChangeXMLPath(string pathToXML) => this.pathToXML = pathToXML;

        private void ChangeOutputPath(string pathToOutput) => this.pathToOutput = pathToOutput;

        //for disposing our object
        public void Dispose()
        {
            writer.Dispose();
            pathToOutput = null;
            pathToXML = null;
        }
    }
}
