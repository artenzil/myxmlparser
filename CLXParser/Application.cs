﻿using System;
using CLXParser.Code;

namespace CLXParser
{
    /// <summary>
    /// Console version of parcer
    /// </summary>
    public static class ConsoleApp
    {
        /// <summary>
        /// Start point of console version
        /// </summary>
        /// <param name="args">Command line arguments</param>
        public static void Main(params string[] args)
        {
            Console.WriteLine(Run(args) == true ? "Complete!" : "Failed! Check logs to more information\nor validate your arguments");
            Console.ReadKey();
        }

        /// <summary>
        /// Start parcing point. Used by console and GUI versions of application
        /// </summary>
        /// <param name="args">Command line arguments</param>
        /// <returns>true, if parcing complete without any exception</returns>
        public static bool Run(params string[] args)
        {
            string pathToXML = "";
            string pathToOutputFile = "";
            string[] splittedString; // using for splitted command line argument
            foreach (var arg in args)
            {
                splittedString = arg.Split('=');

                //in this case, we remove any whitespaces from start and end of command line argument. But, if user sends
                //some repeated arguments, like xml or output (two and more), this code will use last switched argument
                switch (splittedString[0].Trim()) 
                {
                    case "xml":
                        {
                            pathToXML = splittedString[1].Trim();
                            break;
                        }
                    case "output":
                        {
                            pathToOutputFile = splittedString[1].Trim();
                            break;
                        }
                    default: break; // if user have wrongtyped any commandline arg, or if he really wants to give wrong information for parcer
                }
            }
            if (pathToXML == "" || pathToOutputFile == "") return false; // if user lost his path to xml or path to output folder (or both of them)
            Parser parser = Parser.GetXmlParser(pathToXML, pathToOutputFile); //Create parser object
            try
            {
                var result = parser.ParseXML(); //get result for our parsing (true or false)
                parser.Dispose(); //Disposing our parcer and inner using objects
                GC.Collect(2, GCCollectionMode.Forced); //Collect all garbage (For future, if we want using this parser again without restart the application)
                GC.WaitForPendingFinalizers(); // Waiting, while GC complete the cleaning operation
                return result; // return our result
            }
            catch(Exception)
            {
                return false;
            }
        }
    }
}
