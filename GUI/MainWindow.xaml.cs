﻿using System.Windows;
using System.Windows.Forms;
using static CLXParser.ConsoleApp; //for running our parcer

namespace GUI
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        //Start application button
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            XMLBox.IsEnabled = false;
            OutBox.IsEnabled = false;
            if (Run(new string[] { $"xml={XMLBox.Text}", $"output={OutBox.Text}" }))
            {
                System.Windows.Forms.MessageBox.Show("Complete!");
            }
            else System.Windows.Forms.MessageBox.Show("Something gets wrong! Check logs to more information\n" +
                                                            "or check your xml and output fields");
            OutBox.IsEnabled = true;
            XMLBox.IsEnabled = true;
        }

        //Load path to xml file
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openXML = new OpenFileDialog() { AddExtension = true, Filter = "XML file |*.xml;*.XML;", Multiselect = false,
                                                            ValidateNames = true};
            if (openXML.ShowDialog() == System.Windows.Forms.DialogResult.Cancel) return;
            XMLBox.Text = openXML.FileName ?? string.Empty;
        }

        //Load path to output folder
        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            using (var fbd = new FolderBrowserDialog())
            {
                DialogResult result = fbd.ShowDialog();
                if (result == System.Windows.Forms.DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    OutBox.Text = fbd.SelectedPath;
                }
            }
        }
    }
}
